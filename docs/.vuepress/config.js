const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  title: 'Design Library',
  description: 'Powered by Yodel',
  themeConfig: {
    logo: '/yo.png',
    lastUpdated: 'Last updated',
    // docsDir: 'docs',
    // editLinks: true,
    editLinkText: 'Recommend a change',
    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Documentation',
        items: [
          {
            text: 'Components',
            link: '/doc/#components'
          },
          {
            text: 'Content',
            link: '/doc/#content'
          }
        ]
      }
    ],
    plugins: [
      '@vuepress/active-header-links'
    ]
  },
  plugins: [['vuepress-plugin-code-copy', true]],
  postcss: {
    plugins: [
        require('tailwindcss')('./tailwind.config.js'),
        require('autoprefixer'),
    ]
  },
  sass: {
    prependData: `
        @import "@styles/index.sass";
      `
  },
  configureWebpack: {
    resolve: {
      alias: {
        "@styles": path.resolve(__dirname, "./styles")
      }
    },
    plugins: [
      new CopyWebpackPlugin([
        {
          from: path.resolve(__dirname, '../sass/index.scss'),
          to: 'yo/index.scss'
        }
      ])
    ]
  }
}
