---
sidebar: auto
---
# Documentation

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Components

### Buttons

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

components/button.md

## Content

### Table

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

<spacer/>
<content-table/>

::: details View example code
```html
<table class="w-full">
    <thead>
        <tr class="text-xs">
            <th class="pb-4 font-normal text-left">
                Campaign Name
            </th>
            <th class="pb-4 font-normal">
                DID
            </th>
            <th class="pb-4 font-normal">
                Active
            </th>
            <th class="pb-4 font-normal">
                Commands
            </th>
        </tr>
    </thead>
    <tbody>
        <tr class="bg-blueGray-100">
            <td class="px-6 py-4 text-xs font-semibold">
                Official trial - from [ Auto Insurance Standard FF 1274 ] down to [ Medicare Supp Vetting FF 979 ] , [ Lift Chair Vetting FF 1304 ] [ Bathroom remodel Division (K) FF 1031 ]
            </td>
            <td class="px-6 py-4">
                8669824063
            </td>
            <td class="px-6 py-4">
                <i class="far fa-check-square text-green-500"></i>
            </td>
            <td class="px-6 py-4">
                <a class="btn btn-primary" href="#">Show</a>
            </td>
        </tr>
        <tr class="bg-blueGray-100">
            <td class="px-6 py-4 text-xs font-semibold">
                Developer Test Campaign
            </td>
            <td class="px-6 py-4">
                8885551234
            </td>
            <td class="px-6 py-4">
                <i class="far fa-check-square text-green-500"></i>
            </td>
            <td class="px-6 py-4">
                <a class="btn btn-primary" href="#">Show</a>
            </td>
        </tr>
        <tr class="bg-blueGray-100">
            <td class="px-6 py-4 text-xs font-semibold">
                Roberto's Test Campaign
            </td>
            <td class="px-6 py-4">
                8004569876
            </td>
            <td class="px-6 py-4">
                <i class="far fa-check-square text-green-500"></i>
            </td>
            <td class="px-6 py-4">
                <a class="btn btn-primary" href="#">Show</a>
            </td>
        </tr>
    </tbody>
</table>
```
:::