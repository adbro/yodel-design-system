---
home: true
heroImage: /yo.png
actionText: Download
actionLink: ./yo/index.scss
footer: © 2021 allrights Reserved by Yodel™ Technology Services
---

# Introduction

**YO.library** integrates with the [Tailwind CSS](https://tailwindcss.com/) framework including completion for Tailwind classes in HTML files and completion suggestions for pseudo-class variants, preview of the resulting CSS on hovering over classes in HTML and CSS files or on autocompletion. YO.library recognizes tailwind.config.js files and provides completion based on customization you make to them.



## Before you start

Make sure you have [Node.js](https://nodejs.org/en/) on your computer.



### Install Tailwind CSS

1. Open the embedded Terminal and type:

``` npm install tailwindcss postcss autoprefixer ```

2. To configure your Tailwind CSS installation, generate a tailwind.config.js configuration file in the root of your project. In the embedded Terminal type:

``` npx tailwindcss init ```

Learn more from the [Tailwind CSS official website](https://tailwindcss.com/).



## Complete Tailwind classes

**YO.library** autocompletes Tailwind classes in HTML files and in CSS files after the ``` @apply ``` directive.

<style lang="scss">
    h2, h3, h4{
        margin-top: 40px;
        margin-bottom: 10px;
    }
</style>